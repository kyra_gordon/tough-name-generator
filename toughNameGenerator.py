import json
import random

def load(path):
	with open(path,encoding='utf-8') as data:
		return json.load(data)

words = load("data.json")

def nthName():
    return "The " + random.choice(words["lineage"])

def coin():
    return random.randint(1, 2)

def dice():
    return random.randint(1, 6)

# def titleVar():
#     return random.choice(words["titles"])
#
# def suffixVar():
#     return random.choice(words["suffix"])

def tc(layout):
        if (layout == 1):
            return random.choice(words["names"]) + random.choice(words["names"])
        elif (layout == 2):
            return random.choice(words["names"]) + " " + random.choice(words["names"]) + random.choice(words["names"])
        elif (layout == 3):
            return random.choice(words["names"]) + random.choice(words["names"]) + " " + random.choice(words["names"]) + random.choice(words["names"])
        elif (layout == 4):
            return random.choice(words["names"]) + random.choice(words["names"]) + " " + random.choice(words["names"]) + " " + random.choice(words["names"]) + random.choice(words["names"])
        elif (layout == 5):
            return random.choice(words["names"]) + random.choice(words["names"]) + " " + random.choice(words["names"])
        elif (layout == 6):
            return random.choice(words["names"]) + " " + random.choice(words["names"])

def doer():
    return "The " + random.choice(words["things"]) + " " + random.choice(words["actions"])

def get():
    layout = dice()
    linVarCh = coin()
    sc = coin()
    linc = dice()
    if (linc == 1):
        return tc(layout) + " " + nthName()
    elif (linc == 2):
        return tc(layout) + " " + nthName() + ": " + doer()
    else:
        return tc(layout)

if __name__ == "__main__":
    toughName = get()
    print("Y O U R   T O U G H   N A M E   I S:   \n" + str(toughName))
    toughName = toughName + "\n"
    f = open('log.txt', 'a')
    with open('log.txt', 'a') as logfile:
        logfile.write(toughName)
    input()
