# Tough Name Generator

This software generates a tough-sounding name and appends it to a text file.
If you use this, please credit me or put money inside of my pockets:

- [Paypal](paypal.me/kyragordon)
- [Patreon](patreon.com/kyrart)

----
## Examples

```
vicerhino botscold
fangslam slashpiledriver
missileass carbon
juggernautkill The 2nd: The skeleton headbutter
duct diamondhemorrhage The 15th
tronstomper butt botgear The 20th: The tree imploder
liftbust pump stabbermuscle
squeezepiledriver rhino squeezerboot
slicesky bustbench The 1st
juggernautsplode concretekiller
doomlift The 1st: The bone slayer
yell slice
raven vice The 21st
rough snare
stallion godsmeat
anvillaser guitarist
bleedvalkerie trucker
stomper pumpersplode
bull kill
drummerbuff calibre rexaxe
burger ace
kick hornanvil
burgersqueezer slabjaguar
spinecannon sky
load bleed The 6th
guitaristgym The 4th: The bone headbutter
bruisenut diamondbiff
cannon deadliftpumper The 8th: The anvil smasher
godsscream stomperpiledriver The nth: The skeleton exploder
stormermeat turbo The 12th: The groin smasher
chinwolf buff The 1st: The vehicle kicker
snarebutt torpedo asscrunt
armtron lion rexiron
slashduke
sky missile The 11th: The tree kicker
stabber fridgearm The 5th: The house headbutter
buffsqueezer
ironhammer tornadosky
breaksteel pumpbicep
blood anvilspine The 12th: The tree smasher
slicerocket
meatharpoon
battlepump ductgrip The 11th: The tree headbutter
wolf killerscold
baconlift shitterpuncher
shaverjaguar
```

----
## License

MIT, see LICENSE for details.
